module de.grogra.categorizedExporter {
	exports de.grogra.categorizedExporter;
	exports de.grogra.categorizedExporter.obj;
	exports de.grogra.categorizedExporter.ply;
	
	requires graph;
	requires imp;
	requires imp3d;
	requires math;
	requires platform;
	requires platform.core;
	requires utilities;
	requires vecmath;
	requires xl.core;
}
