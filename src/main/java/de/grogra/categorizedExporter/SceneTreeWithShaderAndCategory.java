package de.grogra.categorizedExporter;

import java.util.ArrayList;

import de.grogra.graph.EdgePattern;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphState;
import de.grogra.graph.Path;
import de.grogra.graph.impl.Edge;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTreeWithShader;
import de.grogra.imp3d.objects.SceneTreeWithShader.Leaf;
import de.grogra.imp3d.shading.Interior;
import de.grogra.imp3d.shading.Shader;

public abstract class SceneTreeWithShaderAndCategory extends SceneTreeWithShader{
	private ArrayList<Integer> catIDs;
	public SceneTreeWithShaderAndCategory(View3D scene) {
		super(scene);
		// TODO Auto-generated constructor stub
	}
	
	public static class CatLeaf extends SceneTreeWithShader.Leaf
	{
		
		public Integer[] catIDs = null; 
		/**
		 * The interior of the object of this leaf.
		 */
		public CatLeaf (Object object, boolean asNode, long pathId)
		{
			super (object, asNode, pathId);
		}

		public void setCatIDs(ArrayList<Integer> ids) 
		{
			if(ids!=null) {
				this.catIDs = new Integer[ids.size()];
				this.catIDs = ids.toArray(this.catIDs);				
			}
		}
		
	}
	
	@Override
	protected void init (SceneTree.Leaf leaf)
	{
		super.init (leaf);
		catIDs = getCatIDs(leaf.object);
		((CatLeaf) leaf).setCatIDs(catIDs);
	}

	
	private void getAllRefinements(de.grogra.graph.impl.Node n, ArrayList<Integer> result) {
		for(Edge e = n.getFirstEdge(); e != null; e = e.getNext(n)) {
			if(e.getTarget()==n && e.getEdgeBits()==Graph.REFINEMENT_EDGE) {
				int id=(int)((de.grogra.graph.impl.Node ) e.getSource()).getId();
				if(!result.contains(id)) {
					getAllRefinements(e.getSource(),result);
					result.add(id);	
				}
			}
		}
	}
	
	private ArrayList<Integer> getCatIDs(Object object) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		de.grogra.graph.impl.Node n = (de.grogra.graph.impl.Node ) object;
		getAllRefinements(n,result);
		if(result.size()==0) {
			return null;
		}
		return result;
	}

}
